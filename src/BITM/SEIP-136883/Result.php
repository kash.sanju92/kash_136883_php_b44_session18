<?php

namespace App;


class Result extends Database
{
    private $id;
    private $name;
    private $roll;
    private $markBang;
    private $markEng;
    private $markMath;

    private $gradeBang;
    private $gradeEng;
    private $gradeMath;

    public function setData($postData)
    {

        if(array_key_exists("id", $postData))
        {
            $this->id = $postData['id'];

        }

        if(array_key_exists("name", $postData))
        {
            $this->name = $postData['name'];

        }

        if(array_key_exists("roll", $postData))
        {
            $this->roll = $postData['roll'];

        }

        if(array_key_exists("markBang", $postData))
        {
            $this->markBang = $postData['markBang'];

            $this->gradeBang = $this->convertMark2Grade($this->markBang);

        }

        if(array_key_exists("markEng", $postData))
        {
            $this->markEng = $postData['markEng'];

            $this->gradeEng = $this->convertMark2Grade($this->markEng);

        }

        if(array_key_exists("markMath", $postData))
        {
            $this->markMath = $postData['markMath'];

            $this->gradeMath = $this->convertMark2Grade($this->markMath);

        }


    }

    public function convertMark2Grade($mark)
    {
       switch($mark){

           case $mark>79: return "A+";
           case $mark>74: return "A";
           case $mark>69: return "A-";
           case $mark>64: return "B+";
           case $mark>59: return "B";
           case $mark>54: return "B-";
           case $mark>49: return "C+";
           case $mark>44: return "C";
           case $mark>39: return "D";

           default: return "F";

       }


    }

    public function store()
    {
        $arrayData = array($this-> name, $this-> roll, $this-> markBang, $this-> markEng, $this-> markMath, $this-> gradeBang,
            $this-> gradeEng, $this-> gradeMath);


        $sql = "Insert into result(name, roll, mark_bangla, mark_english, mark_math, grade_bangla, grade_english, grade_math)
        VALUES (?,?,?,?,?,?,?,?)";

        $STH = $this-> DBH->prepare($sql);

        $success = $STH-> execute($arrayData);

        if($success)
        {
            Message:: message("Data inserted successfully<br>");
            echo Message:: message();

        }

        else
        {

            Message:: message("Try again. Not inserted. <br>");


        }

        Utility:: redirect("Info.php");

    }



}